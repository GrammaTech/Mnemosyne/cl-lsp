(defpackage :cl-lsp/cl-lsp-server
  (:use :cl
        :lsp-server/protocol
        :lsp-server/protocol-util
        :lsp-server/lsp-server
        :cl-lsp/slime
        :cl-lsp/swank
        :cl-lsp/formatting
        :lsp-server.lem-base)
  (:import-from :cl-lsp.lem-lisp-syntax.syntax-table
                :*syntax-table*)
  (:import-from :cl-lsp.lem-lisp-syntax.enclosing
                :search-local-definition)
  (:import-from :fset :convert)
  (:export :cl-lsp-server
           :*cl-lsp-server*))
(in-package :cl-lsp/cl-lsp-server)

(defclass cl-lsp-server (lsp-server)
  ())

(defvar *cl-lsp-server*
  (make-instance 'cl-lsp-server))

(define-class-method "textDocument/didOpen" cl-lsp-server
    (params |DidOpenTextDocumentParams|)
  (multiple-value-prog1 (call-next-method)
    (let* ((doc (slot-value params '|textDocument|))
           (uri (slot-value doc '|uri|))
           (buffer (get-buffer-from-uri uri)))
      (setf (buffer-syntax-table buffer)
            *syntax-table*))))

(define-class-method "initialize" cl-lsp-server (params |InitializeParams|)
  (setf (initialize-params*) params)
  (convert 'hash-table
           (make-instance
            '|InitializeResult|
            :|capabilities|
            (make-instance
             '|ServerCapabilities|
             :|textDocumentSync| (progn
                                   #+(or)
                                   (make-instance
                                    '|TextDocumentSyncOptions|
                                    :|openClose| t
                                    :|change| |TextDocumentSyncKind.Incremental|
                                    :|willSave| 'yason:false
                                    :|willSaveWaitUntil| 'yason:false
                                    :|save| (make-instance '|SaveOptions| :|includeText| t))
                                   |TextDocumentSyncKind.Incremental|)
             :|hoverProvider| t
             :|completionProvider| (make-instance
                                    '|CompletionOptions|
                                    :|resolveProvider| nil
                                    :|triggerCharacters| (loop :for code
                                                               :from (char-code #\a)
                                                                 :below (char-code #\z)
                                                               :collect (string (code-char code))))
             :|signatureHelpProvider| (make-instance
                                       '|SignatureHelpOptions|
                                       :|triggerCharacters| (list " "))
             :|definitionProvider| t
             :|referencesProvider| t
             :|documentHighlightProvider| t
             :|documentSymbolProvider| t
             :|workspaceSymbolProvider| t
             :|documentFormattingProvider| t
             :|documentRangeFormattingProvider| t
             :|documentOnTypeFormattingProvider| (make-instance
                                                  '|DocumentOnTypeFormattingOptions|
                                                  :|firstTriggerCharacter| (string #\Newline))
             :|renameProvider| t))))

(define-class-method "initialized" cl-lsp-server (params)
  (swank-init)
  nil)

(define-class-method "textDocument/completion" cl-lsp-server (params |TextDocumentPositionParams|)
  (with-text-document-position (point) params
    (with-point ((start point)
                 (end point))
      (skip-symbol-backward start)
      (skip-symbol-forward end)
      (let ((result
             (fuzzy-completions
              (points-to-string start end)
              (search-buffer-package point))))
        (when result
          (destructuring-bind (completions timeout) result
            (declare (ignore timeout))
            (convert 'hash-table
             (make-instance
              '|CompletionList|
              :|isIncomplete| nil
              :|items| (loop :for completion :in completions
                             :collect (make-instance
                                       '|CompletionItem|
                                       :|label| (first completion)
                                       ;:|kind|
                                       :|detail| (fourth completion)
                                       ;:|documentation|
                                       ;:|sortText|
                                       ;:|filterText|
                                       ;:|insertText|
                                       ;:|insertTextFormat|
                                       :|textEdit| (make-instance
                                                    '|TextEdit|
                                                    :|range| (make-lsp-range start end)
                                                    :|newText| (first completion))
                                       ;:|additionalTextEdits|
                                       ;:|command|
                                       ;:|data|
                                       ))))))))))

(define-class-method "textDocument/hover" cl-lsp-server (params |TextDocumentPositionParams|)
  (with-text-document-position (point) params
    (let* ((symbol-string (symbol-string-at-point* point))
           (describe-string
            (describe-symbol symbol-string
                             (search-buffer-package point))))
      (convert 'hash-table
       (if describe-string
           (with-point ((start point)
                        (end point))
             (skip-chars-backward start #'syntax-symbol-char-p)
             (skip-chars-forward end #'syntax-symbol-char-p)
             (make-instance '|Hover|
                            :|contents| describe-string
                            :|range| (make-lsp-range start end)))
           (make-instance '|Hover|
                          :|contents| ""))))))

(defun arglist (point)
  (let ((symbol-string (symbol-string-at-point* point)))
    (when symbol-string
      (operator-arglist symbol-string
                        (search-buffer-package point)))))

(define-class-method "textDocument/signatureHelp" cl-lsp-server (params |TextDocumentPositionParams|)
  (with-text-document-position (point) params
    (let ((arglist (arglist point)))
      (convert 'hash-table
       (make-instance
        '|SignatureHelp|
        :|signatures| (when arglist
                        (list (make-instance
                               '|SignatureInformation|
                               :|label| arglist))))))))

(defun xref-location (xref)
  (optima:match xref
    ((list _
           (list :location
                 (list :file file)
                 (list :position offset)
                 (list :snippet _)))
     (convert 'hash-table (file-location file offset)))))

(defun xref-locations-from-definitions (defs)
  (loop :for xref :in defs
        :for location := (xref-location xref)
        :when location
        :collect location))

(define-class-method "textDocument/definition" cl-lsp-server (params |TextDocumentPositionParams|)
  (with-text-document-position (point) params
    (alexandria:when-let ((name (symbol-string-at-point* point)))
      (alexandria:if-let ((p (search-local-definition point name)))
                         (convert 'hash-table (buffer-location p))
        (list-to-object-or-object
         (xref-locations-from-definitions
          (find-definitions name (search-buffer-package point))))))))

(define-class-method "textDocument/references" cl-lsp-server (params |ReferenceParams|)
  (with-text-document-position (point) params
    (let ((symbol-string (symbol-string-at-point* point)))
      (list-to-object-or-object
       (loop :for (type . definitions) :in (xrefs symbol-string
                                                  (search-buffer-package point))
             :nconc (xref-locations-from-definitions definitions))))))

(defun collect-symbol-range (buffer name function)
  (let ((regex (ppcre:create-scanner `(:sequence
                                       (:alternation
                                        (:positive-lookbehind
                                         (:char-class #\( #\) #\space #\tab #\:))
                                        :start-anchor)
                                       ,name
                                       (:alternation
                                        (:positive-lookahead
                                         (:char-class #\( #\) #\space #\tab #\:))
                                        :end-anchor))
                                     :case-insensitive-mode t)))
    (with-point ((point (buffer-start-point buffer)))
      (loop :while (search-forward-regexp point regex)
            :collect (with-point ((start point))
                       (character-offset start (- (length name)))
                       (funcall function (make-lsp-range start point)))))))

(defun symbol-name-at-point (point)
  (alexandria:when-let*
      ((string (symbol-string-at-point* point))
       (name (ignore-errors
              (symbol-name
               (let ((*package* (search-buffer-package point)))
                 (read-from-string string))))))
    name))

(define-class-method "textDocument/documentHighlight" cl-lsp-server (params |TextDocumentPositionParams|)
  (with-text-document-position (point) params
    (list-to-object
     (alexandria:when-let (name (symbol-name-at-point point))
       (collect-symbol-range (point-buffer point) name
                             (lambda (range)
                               (convert 'hash-table
                                (make-instance '|DocumentHighlight|
                                               :|range| range))))))))

(defun type-to-symbol-kind (type)
  #+sbcl
  (case type
    (defvar |SymbolKind.Variable|)
    (defconstant |SymbolKind.Variable|)
    (deftype |SymbolKind.Class|)
    (define-symbol-macro |SymbolKind.Variable|)
    (defmacro |SymbolKind.Function|)
    (define-compiler-macro |SymbolKind.Function|)
    (defun |SymbolKind.Function|)
    (defgeneric |SymbolKind.Method|)
    (defmethod |SymbolKind.Method|)
    (define-setf-expander |SymbolKind.Function|)
    (defstruct |SymbolKind.Class|)
    (define-condition |SymbolKind.Class|)
    (defclass |SymbolKind.Class|)
    (define-method-combination |SymbolKind.Function|)
    (defpackage |SymbolKind.Namespace|)
    (:deftransform |SymbolKind.Function|)
    (:defoptimizer |SymbolKind.Function|)
    (:define-vop |SymbolKind.Function|)
    (:define-source-transform |SymbolKind.Function|)
    (:def-ir1-translator |SymbolKind.Function|)
    (declaim |SymbolKind.Function|)
    (:define-alien-type |SymbolKind.Function|)
    (otherwise
     |SymbolKind.Function|))
  #-sbcl
  |SymbolKind.Function|)

(defun xref-to-symbol-information (name xref buffer-file)
  (optima:match xref
    ((list (cons type _)
           (list :location
                 (list :file file)
                 (list :position position)
                 (list :snippet _)))
     (when (and (probe-file file)
                (or (null buffer-file)
                    (equal file buffer-file)))
       (make-instance '|SymbolInformation|
                      :|name| name
                      :|kind| (type-to-symbol-kind type)
                      :|location| (file-location file position))))))

(defun symbol-informations (name package buffer-file)
  (loop :for xref :in (find-definitions name package)
        :for info := (xref-to-symbol-information name xref buffer-file)
        :when info
        :collect info))

(defun document-symbol (buffer)
  (let ((symbol-informations '())
        (used (make-hash-table :test 'equal))
        (package (search-buffer-package (buffer-start-point buffer)))
        (buffer-file (buffer-filename buffer)))
    (map-buffer-symbols
     buffer
     (lambda (symbol-string)
       (unless (gethash symbol-string used)
         (setf (gethash symbol-string used) t)
         (dolist (si (symbol-informations symbol-string package buffer-file))
           (push si symbol-informations)))))
    (if (null symbol-informations)
        (vector)
        (mapcar (alexandria:curry #'convert 'hash-table)
                symbol-informations))))

(define-class-method "textDocument/documentSymbol" cl-lsp-server (params |DocumentSymbolParams|)
  (let* ((text-document (slot-value params '|textDocument|))
         (uri (slot-value text-document '|uri|))
         (buffer (get-buffer uri)))
    (when buffer
      (document-symbol buffer))))

(define-class-method "textDocument/formatting" cl-lsp-server (params |DocumentFormattingParams|)
  (with-slots (|textDocument| |options|) params
    (let ((buffer (get-buffer-from-uri (slot-value |textDocument| '|uri|))))
      (buffer-formatting buffer |options|))))

(define-class-method "textDocument/rangeFormatting" cl-lsp-server (params |DocumentRangeFormattingParams|)
  (with-slots (|textDocument| |range| |options|) params
    (with-slots (|start| |end|) |range|
      (with-document-position (start (slot-value |textDocument| '|uri|) |start|)
        (with-point ((end start))
          (move-to-lsp-position end |end|)
          (range-formatting start end |options|))))))

(define-class-method "textDocument/onTypeFormatting" cl-lsp-server (params |DocumentOnTypeFormattingParams|)
  (with-slots (|textDocument| |position| |ch| |options|) params
    (with-document-position (point (slot-value |textDocument| '|uri|) |position|)
      (on-type-formatting point |ch| |options|))))

(define-class-method "textDocument/rename" cl-lsp-server (params |RenameParams|)
  (with-slots (|textDocument| |position| |newName|) params
    (with-document-position (point (slot-value |textDocument| '|uri|) |position|)
      (alexandria:when-let ((name (symbol-name-at-point point)))
        (let* ((buffer (point-buffer point))
               (uri (filename-to-uri (buffer-filename buffer)))
               (edits (collect-symbol-range
                       (point-buffer point)
                       name
                       (lambda (range)
                         (make-instance '|TextEdit|
                                        :|range| range
                                        :|newText| |newName|)))))
          (convert 'hash-table
           (make-instance
            '|WorkspaceEdit|
            :|changes| (alexandria:plist-hash-table (list uri edits))
            ;; :|documentChanges| (list
            ;;                     (make-instance
            ;;                      '|TextDocumentEdit|
            ;;                      :|textDocument| (make-instance
            ;;                                       '|VersionedTextDocumentIdentifier|
            ;;                                       :|version| (buffer-version buffer)
            ;;                                       :|uri| uri)
            ;;                      :|edits| edits))
            )))))))

(define-class-method "workspace/symbol" cl-lsp-server (params |WorkspaceSymbolParams|)
  (let* ((query (slot-value params '|query|))
         (limit 42))
    (list-to-object
     (when (string/= query "")
       (mapcar (alexandria:curry #'convert 'hash-table)
               (loop :with package := (find-package "CL-USER")
                     :repeat limit
                     :for name :in (swank-apropos-list query package)
                     :append (symbol-informations name package nil)))))))
