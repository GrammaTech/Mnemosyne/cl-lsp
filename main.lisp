(defpackage :cl-lsp/main
  (:use :cl
        :lsp-server/lsp-server
        :cl-lsp/cl-lsp-server
        :lsp-server/logger
        :cl-lsp/eval)
  (:export :run-tcp-mode
           :run-stdio-mode))
(in-package :cl-lsp/main)

(defun run-tcp-mode (&key (port 10003) &aux (*server* *cl-lsp-server*))
  (with-open-stream (*error-output* (make-broadcast-stream))
     (with-log-file ("~/lsp-log")
      (log-format "server-listen~%mode:tcp~%port:~D~%" port)
      (jsonrpc:server-listen *cl-lsp-server* :port port :mode :tcp))))

(defun run-stdio-mode (&aux (*server* *cl-lsp-server*))
  (with-log-file ("~/lsp-log")
    (log-format "server-listen~%mode:stdio~%")
    (jsonrpc:server-listen *cl-lsp-server* :mode :stdio)))
