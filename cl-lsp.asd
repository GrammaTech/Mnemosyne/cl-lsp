(load-asd (merge-pathnames "cl-lsp.lem-lisp-syntax.asd" *load-pathname*))

(defsystem "cl-lsp"
  :depends-on ("bordeaux-threads"
               "trivial-gray-streams"
               "swank"
               "cl-ppcre"
               "optima"
               "alexandria"
               "trivial-types"
               "closer-mop"
               "quri"
               "jsonrpc"
               "jsonrpc/transport/tcp"
               "jsonrpc/transport/stdio"
               "yason"
               "lsp-server"
               "cl-lsp.lem-lisp-syntax")
  :serial t
  :components ((:file "gray-streams")
               (:file "swank")
               (:file "slime")
               (:file "formatting")
               (:file "cl-lsp-server")
               (:file "eval")
               (:file "main")))
