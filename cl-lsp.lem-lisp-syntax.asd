(defsystem "cl-lsp.lem-lisp-syntax"
  :depends-on ("lsp-server/lem-base" "cl-ppcre")
  :pathname "lem-lisp-syntax/"
  :serial t
  :components ((:file "indent")
               (:file "syntax-table")
               (:file "enclosing")))
